<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi;
use OnionApi\Config;
use OnionHttp\HttpRequest;
use OnionLib\Debug;


trait FilterParam 
{
	/**
	 * @var \OnionApi\FilterParam
	 */
	private static $oInstance;
	
	/**
	 * @var array
	 */
	protected $aParams;
	
	/**
	 * @var array
	 */
	protected $aFilters;
	
	/**
	 * @var string
	 */
	protected $sConfigPath;


	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }
	

	/**
	 * 
	 * @return \OnionApi\FilterParam
	 */
	private function __construct ()
	{
	}


	/**
	 * 
	 * @return \OnionApi\FilterParam
	 */
    public static function getInstance () : FilterParam
    {
		if (self::$oInstance === null)
		{
            self::$oInstance = new self();
		}		

        return self::$oInstance;
	}
	
	
	/**
	 *
	 * @return array
	 */
	public function getParamsFilters() : array
	{
		if (is_file($this->sConfigPath . DS . 'api-validate.php'))
		{
			return include($this->sConfigPath . DS . 'api-validate.php');
		}
		else
		{
			return Config::getOptions('params');
		}
	}
	
	
	/**
	 * 
	 * @param \OnionHttp\HttpRequest $poRequest
	 */
	public function setParams (HttpRequest $poRequest) : void
	{
		$this->aParams['GET'] = $poRequest->getQueryParams();
		$this->aParams['POST'] = $poRequest->getPostParams();
		$this->aParams['ARG'] = $poRequest->getArgvParams();
	}
	
	
	/**
	 *
	 */
	public function validateParams () : void
	{
		Debug::debug($this->aParams);
		
		$this->validateParamsType('GET');
		$this->validateParamsType('POST');
		$this->validateParamsType('ARG');
		
		Debug::debug($this->aParams);
	}
	
	
	/**
	 *
	 * @param string $psType
	 */
	public function validateParamsType (string $psType = 'GET') : void
	{
		if (isset($this->aParams[$psType]) && is_array($this->aParams[$psType]))
		{
			foreach ($this->aParams[$psType] as $lsVar => $lsValue)
			{
				if (!$this->validateValue($lsVar, $lsValue, $psType))
				{
					unset($this->aParams[$psType][$lsVar]);
				}
			}
		}
	}
	
	
	/**
	 * @param string $psVar
	 * @param string $psValue
	 * @param string $psType
	 * @return bool
	 */
	public function validateValue (string $psVar, string $psValue, string $psType = 'GET') : bool
	{
		if (isset($this->aFilters[$psType][$psVar]))
		{
			$lsFilter = $this->aFilters[$psType][$psVar];
			
			if (empty($lsFilter) || preg_match("/$lsFilter/", $psValue))
			{
				return true;
			}
		}
		
		return false;
	}
}