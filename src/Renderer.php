<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi;
use OnionApi\Abstracts\AbstractHandler;
use OnionApi\ServerMiddleware\RequestHandlerInterface;
use OnionHttp\HttpRequest;
use OnionHttp\HttpResponse;
use OnionLib\Debug;
use OnionLib\Prompt;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class Renderer extends AbstractHandler implements RequestHandlerInterface
{
	/**
	 * @var \OnionApi\ServerMiddleware\RequestHandlerInterface
	 */
    private static $oInstance;


	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }
	

	/**
	 * 
	 * @return \OnionApi\Renderer
	 */
	private function __construct ()
	{
	}


	/**
	 * 
	 * @return \OnionApi\ServerMiddleware\RequestHandlerInterface
	 */
    public static function getInstance () : RequestHandlerInterface
    {
		if (self::$oInstance === null)
		{
            self::$oInstance = new self();
		}

        return self::$oInstance;
    }


	/**
	 * 
	 * @param ServerRequestInterface $poRequest
     * @param ResponseInterface $poResponse
	 * @param RequestHandlerInterface|null $poNextHandler
	 * @return ResponseInterface
	 */
	public function handle (ServerRequestInterface $poRequest, ResponseInterface $poResponse, ?RequestHandlerInterface $poNextHandler = null) : ResponseInterface
	{
        $this->renderResponse($poRequest, $poResponse);

		return parent::handle($poRequest, $poResponse, $poNextHandler);
    }


	/**
	 * 
	 * @param \OnionHttp\HttpRequest $poRequest
	 * @param \OnionHttp\HttpResponse|array|object|string $pmResponse
	 */
	public function renderResponse (HttpRequest $poRequest, $pmResponse) : void
	{
		if (PHP_SAPI == "cli")
		{
			$this->renderResponseCli($pmResponse);
		}
		else 
		{
			$this->renderResponseHttp($poRequest, $pmResponse);
		}
	}


	/**
	 * 
	 * @param \OnionHttp\HttpResponse|array|object|string $pmResponse
	 */
	public function renderResponseCli ($pmResponse) : void
	{
		$lsBody = '';

		if (($pmResponse instanceof HttpResponse))
		{
			$lsBody = (string)$pmResponse->getBody();
		}
		elseif (is_array($pmResponse))
		{
			$lsBody = json_encode($pmResponse);
		}
		elseif (is_object($pmResponse))
		{
			$lsBody = (string)$pmResponse;
		}
		else
		{
			$lsBody = (string)$pmResponse;
		}
		
		if (strstr($lsBody, '"status":"fail"'))
		{
			Prompt::echoError($lsBody);
		}
		elseif (strstr($lsBody, '"status":"success"'))
		{
			Prompt::echoSuccess($lsBody);
		}
		else
		{
			echo $lsBody;
		}
	}


	/**
	 * 
	 * @param \OnionHttp\HttpRequest $poRequest
	 * @param \OnionHttp\HttpResponse|array|object|string $pmResponse
	 */
	public function renderResponseHttp (HttpRequest $poRequest, $pmResponse) : void
	{
		$lsBody = '';

		if (!($pmResponse instanceof HttpResponse))
		{
			if (is_array($pmResponse))
			{
				$lsBody = json_encode($pmResponse);
			}
			elseif (is_object($pmResponse))
			{
				$lsBody = (string)$pmResponse;
			}
			else
			{
				$lsBody = $pmResponse;
			}
			
			$pmResponse= new HttpResponse(200);
			$pmResponse->write($lsBody);
		}
		
		$pmResponse->withHeader('Access-Control-Allow-Origin', "*");
		$pmResponse->withHeader('Access-Control-Allow-Methods', "GET,POST,PUT,DELETE,OPTIONS,HEAD,PURGE,LOCK,UNLOCK");
		$pmResponse->withHeader('Access-Control-Allow-Headers', "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		
		if ($pmResponse->getHeader('Content-Type') === null)
		{
			$laAccept = explode(",", $poRequest->getHeader('HTTP_ACCEPT')[0]);
			Debug::debug($laAccept);
			$pmResponse->withHeader('Content-Type', $laAccept[0]);			
		}
	
		$pmResponse->render();		
	}    
}    