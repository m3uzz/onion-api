<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi;
use OnionLib\Help as LibHelp;


class Help extends LibHelp
{
    /**
	 * 
	 * @return \OnionApi\Help
	 */
	public function __construct ()
	{
		$this->setAbout();

		$this->setUsage();
		
		return $this;
	}


	/**
	 * 
	 */
	public function setAbout () : void
	{
		$this->set("        *** m3uzz OnionApi - Version: " . Application::API_VERSION . " ***        ", self::PURPLE, self::BGBLACK);
		$this->set("\n");
		$this->set("AUTHOR:  Humberto Lourenço <betto@m3uzz.com>             ", self::CYAN, self::BGBLACK);
		$this->set("\n");
		$this->set("LINK:    http://github.com/m3uzz/onion-api               ", self::CYAN, self::BGBLACK);
		$this->set("\n");
		$this->set("LICENCE: http://www.opensource.org/licenses/BSD-3-Clause ", self::CYAN, self::BGBLACK);
		$this->set("\n\n");
	}


	/**
	 * 
	 */
	public function setUsage () : void
	{
		$this->set("Usage: \n", self::BROWN, "", self::B);
		$this->set("  $ ./onionapi [-u=|--uri=] [-M=|--method=] [-H=|--header=] [-b=|--body=] [paramName=] [options]\n", self::GREEN, "", self::I);
		$this->set("  $ ./onionapi [-u=module/controller/action] [param1=<value1> [paramN=<valueN>]] [options]\n", self::GREEN, "", self::I);
		$this->set("  $ ./onionapi [-u=module/controller/action] [--method=GET] [[--header=HTTP_ACCEPT:text/plan] [--header=HTTP_TOKEN:abc]] [options]\n", self::GREEN, "", self::I);
		$this->set("  $ ./onionapi [-m=<ModuleName>] --help\n", self::GREEN, "", self::I);
		
		$this->setTopic("Route");
		$this->setLine("-u, --uri=<Service-Path>", "HTTP Service path");
		$this->setLine("-m, --module=<Module>", "Module name");
		$this->setLine("-c, --controller=<Controller>", "Controller name");
		$this->setLine("-a, --action=<Action>", "Action name");

		$this->setTopic("HTTP Headers");
		$this->setLine("-M, --method=<HTTP-Method>", "HTTP Request Method (default GET)");
		$this->setLine("-H, --header=<HTTP-Header>", "HTTP Request Headers (ex.: HTTP_TOKEN:XXXX)");
		
		$this->setTopic("Body Message");
		$this->setLine("-b, --body=<Body-Message>", "Text Message");
		
		$this->setTopic("Options");
		$this->setLine("-d, --debug", "Activate debug mod (check config/api-config.php if debug is enable)");
		$this->setLine("-e, --error", "Activate php display error");
		$this->setLine("-h, --help", "Show this help");
		$this->setLine("-p, --prompt", "Activate prompt to input params");
		$this->setLine("--no-prompt", "Deactivate prompt to input params");
		$this->setLine("-T, --test", "Activate test mod");
		$this->setLine("-t, --time", "Activate time count");
		$this->setLine("-i, --interactive", "Activate interaction to confirm actions");
		$this->setLine("--non-interactive", "Deactivate interaction to confirm actions");
	}


	/**
	 * 
	 * @param string $psAppName
	 * @param string $psAppVersion
	 */
	public function setAppVersion (string $psAppName, string $psAppVersion) : void
	{
		$this->setTopic($psAppName);
		$this->setLine("Version", $psAppVersion);
	}
}