<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi;
use OnionApi\Abstracts\AbstractHandler;
use OnionApi\ServerMiddleware\RequestHandlerInterface;
use OnionHttp\HttpResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class Dispatcher extends AbstractHandler implements RequestHandlerInterface
{
	/**
	 * @var \OnionApi\ServerMiddleware\RequestHandlerInterface
	 */
    private static $oInstance;


	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }
	

	/**
	 * 
	 * @return \OnionApi\Dispatcher
	 */
	private function __construct ()
	{
	}


	/**
	 * 
	 * @return \OnionApi\ServerMiddleware\RequestHandlerInterface
	 */
    public static function getInstance () : RequestHandlerInterface
    {
		if (self::$oInstance === null)
		{
            self::$oInstance = new self();
		}		

        return self::$oInstance;
    }


	/**
	 * 
	 * @param ServerRequestInterface $poRequest
	 * @param ResponseInterface $poResponse
	 * @param RequestHandlerInterface|null $poNextHandler
	 * @return ResponseInterface
	 */
	public function handle (ServerRequestInterface $poRequest, ResponseInterface $poResponse, ?RequestHandlerInterface $poNextHandler = null) : ResponseInterface
	{		
		if ($poResponse instanceof HttpResponse && $poResponse->isSuccessful())
        {		
			$poResponse = $this->dispatch($poRequest, $poResponse);
		}
		
		return parent::handle($poRequest, $poResponse, $poNextHandler);
	}


	/**
	 * 
	 * @param ServerRequestInterface $poRequest
	 * @return ResponseInterface|null
	 */
	public function dispatch (ServerRequestInterface $poRequest, ?ResponseInterface $poResponse) : ResponseInterface
	{
		$laRoute = $poRequest->getAttribute('route');
		
		//Verificando se o service existe
		if(file_exists($laRoute['service']))
		{
			//Verificando se a classe existe
			if(class_exists($laRoute['class']))
			{
				//Criando o objeto da classe
				$loController = new $laRoute['class']();
				return $loController->handle($poRequest, $poResponse);
			}
			else
			{
				//Se a classe não foi encontrada, retornar 404 not found
				return $this->notFoundResponse($poRequest, 404, "Class Not Found ({$laRoute['controller']})");
			}
		}
		else
		{
			$loResponse = $this->notFoundResponse($poRequest, 404, "Service Not Found ({$laRoute['module']})");

			if (PHP_SAPI == "cli")
			{
				$loHelp = new Help;
				$loHelp->factory(dirname(dirname($laRoute['service'])) . DS . 'config');
				$loHelp->setModuleHelp($loHelp->getModuleHelp($laRoute['module']));
				$loResponse->write("\n\n");
				$loResponse->write($loHelp->display());
			}

			return $loResponse;
        }
	}
	
	
	/**
	 *
	 * @param ServerRequestInterface $poRequest
	 * @param int $pnCode
	 * @param string|array $pmMessage
	 * @return HttpResponse|ResponseInterface
	 */
	public function notFoundResponse (ServerRequestInterface $poRequest, int $pnCode, $pmMessage) : HttpResponse
	{
		$loResponse = new HttpResponse(404);
		
		if (!is_array($pmMessage))
		{
			$pmMessage = [$pmMessage];
		}

        $laBody = [
            'status' => 'fail',
            'statusMessage' => [
                [
                    'method' => $poRequest->getMethod(),
                    'code' => $pnCode,
                    'message' => $pmMessage
                ]
            ],
            'data' => []
        ];
		
		$loResponse->withJson($laBody);
		
		return $loResponse;
	}	
}    