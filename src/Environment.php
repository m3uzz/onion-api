<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi;
use OnionLib\Util;


class Environment
{
	/**
	 * @var \OnionApi\Environment
	 */
	private static $oInstance;

	/**
	 * @var array
	 */
	protected $aConfig = [];

	/**
	 * @var bool
	 */	
	protected $bPhpError = false;
	
	/**
	 * @var bool
	 */	
	protected $bDebug = false;
	
	/**
	 * @var bool
	 */	
	protected $bTime = false;
	
	/**
	 * @var bool
	 */	
	protected $bHelp = false;
	
	/**
	 * @var bool
	 */	
	protected $bTest = false;
	
	/**
	 * @var bool
	 */	
	protected $bPrompt = false;

	/**
	 * @var bool
	 */	
	protected $bInteractive = true;
	

	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }
	

	/**
	 * 
	 * @return \OnionApi\Environment
	 */
	private function __construct()
	{
	}


	/**
	 * 
	 * @return \OnionApi\Environment
	 */
    public static function getInstance () : Environment
    {
		if (self::$oInstance === null)
		{
            self::$oInstance = new self();
		}
		
		$laConfig = Config::loadConfigs();
		
		if (is_array($laConfig))
		{
			self::$oInstance->aConfig = $laConfig;
		}

        return self::$oInstance;
	}	
		
	
	/**
	 * Load the application options
	 *
	 * @param string $psOption
	 * @return string|array|null
	 */
	public function getOptions (string $psOption)
	{
		if (!is_null($psOption) && isset($this->aConfig[$psOption]))
		{
			return $this->aConfig[$psOption];
		}
		
		return null;
	}
	
	
	/**
	 *
	 */
	public function setEnv () : void
	{		
		if (PHP_SAPI == "cli" && isset($_SERVER['argv']) && is_array($_SERVER['argv']))
		{
			foreach ($_SERVER['argv'] as $gsArg)
			{
				if ($gsArg === '--error' || $gsArg === '-e')
				{
					$this->bPhpError = true;
				}
				
				if ($gsArg === '--debug' || $gsArg === '-d')
				{
					$this->bDebug = true;
					$this->bTime = true;
				}
				
				if ($gsArg === '--time' || $gsArg === '-t')
				{
					$this->bTime = true;
				}
				
				if ($gsArg === '--help' || $gsArg === '-h')
				{
					$this->bHelp = true;
				}
				
				if ($gsArg === '--test' || $gsArg === '-T')
				{
					$this->bTest = true;
				}
				
				if ($gsArg === '--prompt' || $gsArg === '-p')
				{
					$this->bPrompt = true;
				}

				if ($gsArg === '--no-prompt')
				{
					$this->bPrompt = false;
				}

				if ($gsArg === '--interactive' || $gsArg === '-i')
				{
					$this->bInteractive = true;
				}

				if ($gsArg === '--non-interactive')
				{
					$this->bInteractive = false;
				}
			}
		}
		else
		{
			if (isset($_GET['error']))
			{
				$this->bPhpError = true;
			}
			
			if (isset($_GET['debug']) || APP_ENV === 'debug')
			{
				$this->bDebug = true;
				$this->bTime = true;
			}
			
			if (isset($_GET['time']))
			{
				$this->bTime = true;
			}
			
			if (isset($_GET['test']))
			{
				$this->bTest = true;
			}
		}
		
		$this->setTimeZone();
		$this->setChmod();
		$this->setEventLog();
		$this->setDebugMod();

		defined('PROMPT') || define('PROMPT', $this->bPrompt);
		defined('INTERACTIVE') || define('INTERACTIVE', $this->bInteractive);
		defined('SHOW_HELP') || define('SHOW_HELP', $this->bHelp);
	}
	
	
	/**
	 *
	 */
	public function setTimeZone () : void
	{
		$lsTimeZone = $this->getOptions('time-zone');
		date_default_timezone_set((string)$lsTimeZone);
	}
	
	
	/**
	 *
	 */
	public function setChmod () : void
	{
		$laSystem = $this->getOptions('system');
		
		if (isset($laSystem['chmod']))
		{
			defined('FILE_CHMOD') || define('FILE_CHMOD', $laSystem['chmod']);
		}
		
		if (isset($laSystem['chown']))
		{
			defined('FILE_CHOWN') || define('FILE_CHOWN', $laSystem['chown']);
		}
		
		if (isset($laSystem['chgrp']))
		{
			defined('FILE_CHGRP') || define('FILE_CHGRP', $laSystem['chgrp']);
		}
	}
	
	
	/**
	 *
	 */
	public function setEventLog () : void
	{
		$laLog = $this->getOptions('log');
		
		defined('ONIONLOGPATH') || define('ONIONLOGPATH', $laLog['folder']);
		defined('ONIONLOGENABLE') || define('ONIONLOGENABLE', Util::toBoolean($laLog['enable']));
	}
	
	
	/**
	 *
	 */
	public function setDebugMod () : void
	{
		$lbDebugMod = false;
		$laDebug = $this->getOptions('debug');
		
		if (Util::toBoolean($laDebug['enable']))
		{
			$lbDebugMod = Util::toBoolean($laDebug['enable']);
		}
		
		if (isset($laDebug['output']))
		{
			defined('DEBUGOUTPUT') || define('DEBUGOUTPUT', $laDebug['output']);
		}
		
		if ($this->bDebug && $lbDebugMod)
		{
			error_reporting(E_ALL);
			ini_set("display_errors", '1');
			defined('DEBUG') || define('DEBUG', true);
			defined('TIMESHOW') || define('TIMESHOW', true);
		}
		else
		{
			if ($this->bPhpError && $lbDebugMod)
			{
				error_reporting(E_ALL);
				ini_set("display_errors", '1');
			}
			else
			{
				error_reporting(null);
				ini_set("display_errors", '0');
			}
			
			defined('DEBUG') || define('DEBUG', false);
		}
		
		if ($this->bTime && $lbDebugMod)
		{
			defined('TIMESHOW') || define('TIMESHOW', true);
		}
		else
		{
			defined('TIMESHOW') || define('TIMESHOW', false);
		}
		
		if ($this->bTest && $lbDebugMod)
		{
			defined('TESTMOD') || define('TESTMOD', true);
		}
		else
		{
			defined('TESTMOD') || define('TESTMOD', false);
		}
	}
}