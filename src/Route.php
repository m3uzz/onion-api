<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi;
use OnionApi\Abstracts\AbstractHandler;
use OnionApi\ServerMiddleware\RequestHandlerInterface;
use OnionHttp\HttpResponse;
use OnionLib\ArrayObject;
use OnionLib\Debug;
use OnionLib\Str;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class Route extends AbstractHandler implements RequestHandlerInterface
{
	/**
	 * @var \OnionApi\ServerMiddleware\RequestHandlerInterface
	 */
	private static $oInstance;

	/**
	 * @var object
	 */
	protected $oLoader;
	
	/**
	 * @var array
	 */
	protected $aModules = [];
	
	/**
	 * @var array
	 */
	protected $aMap = [];	
	
	/**
	 * @var array
	 */
	protected $aModsConf = [];
	
	/**
	 * @var \OnionHttp\Uri
	 */
	protected $oUri = null;


	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }
	

	/**
	 * 
	 * @return \OnionApi\Route
	 */
	private function __construct ()
	{
	}


	/**
	 * 
	 * @param object $poComposerLoader
	 * @param array $paModulesAvailable
	 * @param array $paModulesMap
	 * @return \OnionApi\ServerMiddleware\RequestHandlerInterface
	 */
    public static function getInstance (object $poComposerLoader) : RequestHandlerInterface
    {
		if (self::$oInstance === null)
		{
            self::$oInstance = new self();
		}
		
		self::$oInstance->oLoader = $poComposerLoader;

		$laModsAvailables = Config::getOptions('mods-available');
		$laModMaps = Config::getOptions('modules');

		if (is_array($laModsAvailables))
		{
			self::$oInstance->aModules = $laModsAvailables;
		}
		
		if (is_array($laModMaps))
		{
			self::$oInstance->aMap = $laModMaps;
		}
		
		Application::getInstance()->setRoute(self::$oInstance);
		
        return self::$oInstance;
	}	
	
		
	/**
	 * 
	 * @param string $psProperty
	 * @return mixed
	 */
	public function get (string $psProperty)
	{
		if (property_exists($this, $psProperty))
		{
			$lsMethod = "get{$psProperty}";

			if (method_exists($this, $lsMethod))
			{
				return $this->$lsMethod();
			}
			else
			{
				return $this->$psProperty;
			}
		}
	}
	

	/**
	 * 
	 * @param ServerRequestInterface $poRequest
	 * @param ResponseInterface $poResponse
	 * @param RequestHandlerInterface|null $poNextHandler
	 * @return ResponseInterface
	 */
	public function handle (ServerRequestInterface $poRequest, ResponseInterface $poResponse, ?RequestHandlerInterface $poNextHandler = null) : ResponseInterface
	{
		if ($poResponse instanceof HttpResponse && $poResponse->isSuccessful())
        {
			$this->autoload();
			$poResponse = $this->serviceRoute($poRequest, $poResponse);
		}

		return parent::handle($poRequest, $poResponse, $poNextHandler);
	}

	
	/**
	 *
	 */
	public function autoload () : void
	{
		$this->getModsEnable();
		Debug::debug($this->aMap);
		
		if (is_array($this->aMap))
		{
			foreach ($this->aMap as $lsNamespace => $laPath)
			{
				$this->oLoader->setPsr4($lsNamespace, $laPath);
			}
		}
	}
	
	
	/**
	 * 
	 * @param string $psModule
	 * @return array|null
	 */
	public function getModsConf (?string $psModule = null)
	{
		if (!is_null($psModule))
		{
			if (isset($this->aModsConf[$psModule]))
			{
				return $this->aModsConf[$psModule];
			}
		}
		else 
		{
			return $this->aModsConf;
		}
		
		return null;
	}
    
	
	/**
	 *
	 * @throws \Exception
	 */
	public function getModsEnable () : void
	{
		Debug::debug($this->aModules);
		
		if (is_array($this->aModules))
		{
			foreach ($this->aModules as $lsAlias => $lbEnable)
			{
				if ($lbEnable)
				{
					$lsModulePath = PROJECT_DIR . DS . 'api' . DS . $lsAlias . DS . 'Module.php';
					Debug::debug($lsModulePath);
					
					if (file_exists($lsModulePath))
					{
						require_once $lsModulePath;
						
						$lsClass = "{$lsAlias}\Module";
						Debug::debug($lsClass);
						
						if (class_exists($lsClass))
						{
							//Debug::debug($lsClass . " existe");
							$loModule = new $lsClass();
							
							if (is_object($loModule))
							{
								//Debug::debug($loModule);
								
								if (method_exists($loModule, 'getAutoloaderConfig'))
								{
									$laModule = $loModule->getAutoloaderConfig();
									Debug::debug($laModule);
									
									if (is_array($laModule))
									{
										foreach ($laModule as $lsNamespace => $laPath)
										{
											$this->aMap[$lsNamespace] = $laPath;
										}
									}
								}
								else
								{
									Debug::debug("Method getAutoloadConfig not found");
									throw new \Exception("Method getAutoloadConfig not found!");
								}
								
								if (method_exists($loModule, 'getConfig'))
								{
									$this->aModsConf[$lsAlias] = $loModule->getConfig();
								}
								else
								{
									Debug::debug("Method getConfig not found");
									throw new \Exception("Method getConfig not found!");
								}
							}
							else
							{
								Debug::debug("objeto Module nao criado");
								throw new \Exception("Objeto Module não criado");
							}
						}
						else
						{
							Debug::debug("Class {$lsClass} not found");
							throw new \Exception("Class {$lsClass} not found");
						}
					}
					else
					{
						//Debug::debug("Module {$lsAlias} not found!");
						//throw new \Exception("Module {$lsAlias} not found!");
					}
				}
			}
		}
	}

    
	/**
	 *
	 * @param ServerRequestInterface $poRequest
	 * @param ResponseInterface|null $poResponse
	 * @return ResponseInterface
	 */
	public function serviceRoute (ServerRequestInterface $poRequest, ?ResponseInterface $poResponse = null) : ResponseInterface
	{
		$this->oUri = $poRequest->getUri();
		$lsUri = $this->oUri->getPath();
		Debug::debug($lsUri);
		
		if (isset($lsUri))
		{
			$laRoute = $this->getRoutes($lsUri);

			if (is_array($laRoute))
			{
				if (isset($laRoute['params']) && is_array($laRoute['params']))
				{
					$poRequest->withQueryParams($laRoute['params']);
				}
			}
			else
			{
				//Separando o caminho e verificando quantos elementos tem
				$laRequestUri = explode("/", $lsUri);
				Debug::debug($laRequestUri);

				$lnCount = 0;
				
				//Removendo elementos vazios
				foreach($laRequestUri as $lsPathUrl)
				{
					if(!empty($lsPathUrl))
					{
						$laPathUrl = explode('-', $lsPathUrl);
						$lsValueName = $laPathUrl[0];
						unset($laPathUrl[0]);
						
						if (count($laPathUrl) > 0)
						{
							foreach ($laPathUrl as $lsValue)
							{
								$lsValueName .= ucfirst($lsValue);
							}
						}
						
						$laPath[] = $lsValueName;
						$lnCount++;
					}
				}
				
				if ($lnCount == 0)
				{
					$laPath[0] = 'index';
					$lnCount = 1;
				}
				
				$laRoute['module'] = ucfirst($laPath[0]);
				Debug::debug($laRoute);
				$lsPath = $this->getNamespace($laRoute['module']);

				switch ($lnCount)
				{
					case 1:
						$lsService = $lsPath . DS . 'Controller' . DS . $laRoute['module'] . "Controller.php";
						$lsClass = '\\' . $laRoute['module'] . '\\Controller\\' . $laRoute['module'] . 'Controller';
						
						$laRoute['controller'] = $laRoute['module'];
						$laRoute['action'] = $laPath[0];
						break;
					case 2:
						$lsService = $lsPath . DS . 'Controller' . DS . $laRoute['module'] . "Controller.php";
						$lsClass = '\\' . $laRoute['module'] . '\\Controller\\' . $laRoute['module'] . 'Controller';
						
						$laRoute['controller'] = $laRoute['module'];
						$laRoute['action'] = $laPath[1];
						break;
					default:
						$lsService = $lsPath . DS . 'Controller' . DS .  ucfirst($laPath[1]) . "Controller.php";
						$lsClass = '\\' . $laRoute['module'] . '\\' . 'Controller' . '\\' .  ucfirst($laPath[1]) . "Controller";
						
						$laRoute['controller'] = ucfirst($laPath[1]);
						$laRoute['action'] = $laPath[2];
				}
				
				if (TESTMOD)
				{
					$lsMethod = $laRoute['action'] . 'Test';
				}
				else
				{
					$lsMethod = $laRoute['action'] . 'Action';
				}
				
				$laRoute['service'] = $lsService;
				$laRoute['class'] = $lsClass;
				$laRoute['method'] = $lsMethod;
			}
			
			Debug::debug($laRoute);
			
			$poRequest->withAttribute('route', $laRoute);

			return $poResponse;
		}

		return new HttpResponse(500);
	}
	
	
    /**
     * 
     * @param string $psUri
     * @return array|null
     */
    public function getRoutes (string $psUri) : ?array
    {
    	Debug::debug($this->aModsConf);
    	foreach ($this->aModsConf as $lsAlias => $laConfig)
    	{
    		Debug::debug($laConfig);
    		if (is_array($laConfig) && isset($laConfig['router']['routes']))
    		{
    			$laRoutes = $laConfig['router']['routes'];
    			Debug::debug($laRoutes);
    			if (is_array($laRoutes))
    			{
    				foreach ($laRoutes as $lsModuleName => $laRoute)
    				{
						Debug::debug($lsAlias);
						Debug::debug($lsModuleName);
    					Debug::debug($laRoute);
						Debug::debug($psUri);

    					$laService = $this->checkRoute($lsAlias, $laRoute, $psUri);
    					
    					if (null !== $laService)
    					{
    						Debug::debug($laService);
    						return $laService;
    					}
    				}
    			}
    		}
    	}
    	
    	return null;
    }
    
    
    /**
     * 
     * @param string $psNamespace
     * @param array $paRoute
     * @param string $psUri
     * @return array|null
     */
    public function checkRoute (string $psNamespace, array $paRoute, string $psUri) : ?array
    {
    	$laOptions = $paRoute['options'];
    	
    	$laService['module'] = $psNamespace;
    	$laService['controller'] = $laOptions['defaults']['controller'];
    	$laService['action'] = $laOptions['defaults']['action'];
		Debug::debug($laService);

    	if ($paRoute['type'] == 'Literal')
    	{	
    		$lsPattern = $laOptions['route'];
    	}
    	elseif ($paRoute['type'] == 'Segment') 
    	{
    		foreach ($laOptions['constraints'] as $lsKey => $lsExpression)
    		{
    			$laPattern[] = "/:$lsKey/";
    			$laReplace[] = "(?<$lsKey>$lsExpression)";
    		}
    		
    		$lsPattern = preg_replace(["/\[/","/\]/"], ["(",")*"], $laOptions['route']);
    		$lsPattern = preg_replace($laPattern, $laReplace, $lsPattern);
    		$lsPattern = "$lsPattern";
    	}
    	else 
    	{
    		return null;
    	}
    	
    	Debug::debug("^{$lsPattern}");
    	Debug::debug($psUri);

    	$lbMatch = preg_match("#^$lsPattern#", $psUri, $laMatches);
    	
    	Debug::debug($laMatches);
    	
    	if ($lbMatch)
    	{
    		if (is_array($laMatches))
    		{
    			foreach ($laMatches as $lsKey => $lsValue)
    			{
    				if ($lsKey == 'module')
    				{
    					if (!empty($lsValue))
    					{
    						$laService['module'] = ucfirst(Str::lcfirst($lsValue));
    					}
    				}
    				elseif ($lsKey == 'controller')
    				{
    					if (!empty($lsValue))
    					{
    						$laService['controller'] = ucfirst(Str::lcfirst($lsValue));
    					}
    				}
    				elseif ($lsKey == 'action')
    				{
    					if (!empty($lsValue))
    					{
    						$laService['action'] = Str::lcfirst($lsValue);
    					}
    				}
    				elseif (is_string($lsKey))
    				{
    					$laService['params'][$lsKey] = $lsValue;
    				}
    			}
    		}
    		
    	}
    	else
    	{
    		return null;
    	}
    	
		Debug::debug($psNamespace);
    	$lsPath = $this->getNamespace($psNamespace);
		Debug::debug($lsPath);
		Debug::debug($laService);
		Debug::debug($laService['controller']);

    	$laService['service'] = $lsPath . DS . 'Controller' . DS . $laService['controller'] . 'Controller.php';
    	$laService['class'] = $this->aModsConf[$psNamespace]['controllers']['invokables'][$laOptions['defaults']['__NAMESPACE__'] . '\\' . $laService['controller']];
    	//'\\' . $laOptions['defaults']['__NAMESPACE__'] . '\\' . $laService['controller'] . 'Controller';
    			
    	if (TESTMOD)
    	{
    		$lsMethod = $laService['action'] . 'Test';
    	}
    	else
    	{
    		$lsMethod = $laService['action'] . 'Action';
    	}
    			
    	$laService['method'] = $lsMethod;
    			
    	return $laService;
    }

	
	
	/**
	 * 
	 * @param string $psNamespace
	 * @return string
	 */
	public function getNamespace (string $psNamespace) : string
	{
		$laPrefixes = $this->oLoader->getPrefixes();
		$laPrefixesPsr4 = $this->oLoader->getPrefixesPsr4();
		
		$laPrefixes = ArrayObject::merge($laPrefixes, $laPrefixesPsr4, true);
		
		$lsPath = "";
			
		Debug::debug($psNamespace);
		Debug::debug($laPrefixes);

		if (isset($laPrefixes[ucfirst($psNamespace)]))
		{
			$lsPath = $laPrefixes[ucfirst($psNamespace)][0];
		}
		elseif (isset($laPrefixes[ucfirst($psNamespace) . "\\"]))
		{
			$lsPath = $laPrefixes[ucfirst($psNamespace) . "\\"][0];
		}
			
		return $lsPath;
	}
}