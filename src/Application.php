<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi;
use OnionApi\Abstracts\AbstractHandler;
use OnionApi\ServerMiddleware\RequestHandlerInterface;
use OnionHttp\HttpRequest;
use OnionHttp\HttpResponse;
use OnionLib\Debug;


class Application extends AbstractHandler
{
	const API_VERSION = "3.20.09";


	/**
	 * @var \OnionApi\Application
	 */
	private static $oInstance;

	/**
	 * @var \OnionApi\Route
	 */
	protected $oRoute;

	/**
	 * @var array
	 */
	protected $aPipe = [];

	/**
	 * @var array
	 */
	protected $aPipeNamed = [];

	/**
	 * @var array
	 */
	protected $aPipeNamespace = [];


	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }


	/**
	 * 
	 * @return \OnionApi\Application
	 */
    public static function getInstance () : Application
    {
		if (self::$oInstance === null)
		{
            self::$oInstance = new self();
		}
		
        return self::$oInstance;
	}
	
		
	/**
	 * 
	 * @param string $psProperty
	 * @return mixed
	 */
	public function get (string $psProperty)
	{
		if (property_exists($this, $psProperty))
		{
			$lsMethod = "get{$psProperty}";

			if (method_exists($this, $lsMethod))
			{
				return $this->$lsMethod();
			}
			else
			{
				return $this->$psProperty;
			}
		}
	}
	

	/**
	 * 
	 * @param Route $poRoute
	 */
	public function setRoute(Route $poRoute) : void
	{
		$this->oRoute = $poRoute;
	}


	/**
	 * 
	 * @param RequestHandlerInterface $poHandler
	 */
	public function add (RequestHandlerInterface $poHandler)
	{
		if (count($this->aPipe) > 0)
		{
			$lnKey = array_key_last($this->aPipe);
			$this->aPipe[$lnKey]->setNext($poHandler);
		}
		else
		{
			$this->setNext($poHandler);
		}

		$this->aPipe[] = $poHandler;
		$this->aPipeNamed[get_class($poHandler)] = $poHandler;
		$this->aPipeNamespace[] = get_class($poHandler);
	}


	/**
	 * 
	 * @param object $poLoader
	 */
	public function run (object $poLoader)
	{	
		Environment::getInstance()->setEnv();

		$this->add(Route::getInstance($poLoader));
		$this->add(OriginAuthorization::getInstance());
		$this->add(Authentication::getInstance());
		$this->add(Authorization::getInstance());
		$this->add(Dispatcher::getInstance());
		$this->add(Renderer::getInstance());

		Debug::debug($this->aPipeNamespace);

		$loRequest = HttpRequest::factory();
		$this->debugRequest($loRequest);
		$this->handle($loRequest, new HttpResponse());
		
		Debug::debugTimeEnd("init");
		Debug::displayTime();
	}	

	
	/**
	 * 
	 * @param HttpRequest $poRequest
	 */
	private function debugRequest (HttpRequest $poRequest) : void
	{
		Debug::debugTimeStart("init", false, false, $poRequest->getServerParam('REQUEST_TIME_FLOAT'));
		Debug::debug($poRequest->getUri()->getProperties());
		//Debug::debug($poRequest->getQueryParams());
		//Debug::debug($poRequest->getPostParams());
		Debug::debug($poRequest->getArgvParams());
		//Debug::debug($poRequest->getParsedBody());
		//Debug::debug($poRequest->getCookieParams());
		//Debug::debug($poRequest->getHeaders());
		//Debug::debug($poRequest->getAttributes());
		//Debug::debug($poRequest->getUploadedFiles());
		//Debug::debug($poRequest->getServerParams());
	}	
}