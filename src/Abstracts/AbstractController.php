<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi\Abstracts;
use OnionApi\Application;
use OnionApi\FilterParam;
use OnionApi\Help;
use OnionApi\ServerMiddleware\RequestHandlerInterface;
use OnionHttp\HttpResponse;
use OnionLib\Debug;
use OnionLib\Prompt;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;


abstract class AbstractController extends AbstractHandler implements RequestHandlerInterface
{
	use FilterParam;


	/**
	 * @var \OnionApi\Application
	 */
	protected $oApplication;
	
	/**
	 * @var \OnionHttp\HttpRequest
	 */
	protected $oRequest;
	
	/**
	 * @var \OnionHttp\HttpResponse
	 */
	protected $oResponse;
	
	/**
	 * @var bool
	 */	
	protected $bRestful = false;

	/**
	 * @var object|\OnionApi\Abstracts\AbstractService
	 */	
	protected $oService;

	/**
	 * @var object|\OnionApi\Abstracts\AbstractView
	 */	
	protected $oView;
	
	/**
	 * @var object
	 */	
	protected $oRepository;
	
	/**
	 * @var array
	 */	
	protected $aRepository = [];

	/**
	 * @var string
	 */	
	protected $sControllerPath;
	
	/**
	 * @var string
	 */	
	protected $sServicePath;
	
	/**
	 * @var string
	 */	
	protected $sModelPath;
	
	/**
	 * @var string
	 */	
	protected $sViewPath;
	
	/**
	 * @var string
	 */	
	protected $sConfigPath;
	
	/**
	 * @var string
	 */	
	protected $sViewControllerPath;
	
	/**
	 * @var string
	 */	
	protected $sClass;
	
	/**
	 * @var string
	 */
	protected $sMethod;
	
	/**
	 * @var string
	 */	
	protected $sModule;
	
	/**
	 * @var string
	 */	
	protected $sController;
	
	/**
	 * @var string
	 */	
	protected $sAction;
	
	/**
	 * @var string
	 */	
	protected $sLayoutTemplate;
	
	/**
	 * @var string
	 */	
	protected $sViewTemplate;
	
	/**
	 * @var string
	 */	
	protected $sResoponseType = 'http'; // json, stream
	
	/**
	 * @var int
	 */	
	protected $nStreamRetry = 4000;
	
	/**
	 * @var string
	 */	
	protected $sAccessContol = '*';

	
	/**
	 * 
	 */
	public function __construct ()
	{
	}


	/**
	 * 
	 * @param ServerRequestInterface $poRequest
	 * @param ResponseInterface|null $poResponse 
	 */
	public function initialize (ServerRequestInterface $poRequest, ?ResponseInterface $poResponse = null) : void
	{
		$this->oApplication = Application::getInstance();
		
		$this->oRequest = $poRequest;
				
		$this->oResponse = $poResponse;

		$laRoute = $poRequest->getAttribute('route');
		
		$this->sControllerPath = dirname($laRoute['service']);
		
		$this->sServicePath = dirname(dirname($laRoute['service'])) . DS . 'Service';
		
		$this->sViewPath = dirname(dirname($laRoute['service'])) . DS . 'View';
		
		$this->sConfigPath = dirname(dirname(dirname($laRoute['service']))) . DS . 'config';
		
		$this->sViewControllerPath = dirname(dirname(dirname($laRoute['service']))) . DS . 'view';
		
		$this->sClass = $laRoute['class'];
		
		$this->sMethod = $laRoute['method'];
		
		$this->sModule = $laRoute['module'];
		
		$this->sController = $laRoute['controller'];

		$this->sAction = $laRoute['action'];
		
		$this->aFilters = $this->getParamsFilters();
		
		$this->setParams($poRequest);
		
		Debug::debug($this->aFilters);
		
		$this->validateParams();
		
		//Debug::debug($this);
		
		$lsMethod = 'init';

		if (method_exists($this, $lsMethod))
		{
			$this->{$lsMethod}();
		}
	}

	
	/**
	 * 
	 * @param ServerRequestInterface $poRequest
	 * @param ResponseInterface $poResponse
	 * @param RequestHandlerInterface|null $poNextHandler
	 * @return ResponseInterface
	 */
	public function handle (ServerRequestInterface $poRequest, ResponseInterface $poResponse, ?RequestHandlerInterface $poNextHandler = null) : ResponseInterface
	{
		$this->initialize($poRequest, $poResponse);

		if (!$this->oResponse->isSuccessful())
		{
			return $this->oResponse;
		}

		$lsHelp = $this->help();
		
		if (!is_null($lsHelp))
		{
			return $this->oResponse->write($lsHelp);
		}
		
		if ($this->bRestful)
		{
			$lsMethodAction = $poRequest->getMethod();
			$lsMethodAction .= $this->sMethod;
		}
		else 
		{
			$lsMethodAction = $this->sMethod;
		}
		
		//Verificando se o objeto foi criado e se o metodo existe
		if(method_exists($this, $lsMethodAction))
		{
			//Executando o metodo
			$lmResponse = $this->{$lsMethodAction}();

			if (!$lmResponse instanceof ResponseInterface)
			{
				$lmResponse = $this->oResponse->write((string)$lmResponse);
			}

			return parent::handle($poRequest, $lmResponse);
		}

		//Se o metodo não foi encontrado, retornar 404 not found
		return $this->notFoundResponse($poRequest, 404, "Method Not Found ({$this->sAction})");
	}


	/**
	 *
	 * @param ServerRequestInterface $poRequest
	 * @param int $pnCode
	 * @param string|array $pmMessage
	 * @return HttpResponse|ResponseInterface
	 */
	public function notFoundResponse (ServerRequestInterface $poRequest, int $pnCode, $pmMessage) : HttpResponse
	{
		$loResponse = new HttpResponse(404);
		
		if (!is_array($pmMessage))
		{
			$pmMessage = [$pmMessage];
		}

        $laBody = [
            'status' => 'fail',
            'statusMessage' => [
                [
                    'method' => $poRequest->getMethod(),
                    'code' => $pnCode,
                    'message' => $pmMessage
                ]
            ],
            'data' => []
        ];
		
		$loResponse->withJson($laBody);
		
		return $loResponse;
	}

	
	/**
	 * 
	 * @param bool $pbForce
	 * @param bool $pbGeneral
	 * @param bool $pbDisplay
	 * @return \OnionApi\Help|string|null
	 */
	public function help (bool $pbForce = false, bool $pbGeneral = false, bool $pbDisplay = true)
	{
		if (SHOW_HELP || $pbForce)
		{			
			$loHelp = new Help();
			
			if (method_exists($this, 'moduleHelp'))
			{
				$this->moduleHelp($loHelp);
			}
			
			if (!$pbGeneral)
			{
				$laHelpContent = $loHelp->getActionHelp($this->sModule, $this->sController, $this->sAction);
			
				if (count($laHelpContent) == 0)
				{
					$laHelpContent = $loHelp->getControllerHelp($this->sModule, $this->sController);
				}
			
				$loHelp->setModuleHelp($laHelpContent);
			}
			
			if ($pbDisplay)
			{
				return $loHelp->display();
			}

			return $loHelp;
		}
		
		return null;
	}
	
	
	/**
	 *
	 * @param \OnionApi\Help $poHelp
	 */
	public function moduleHelp (Help $poHelp) : void
	{
		$poHelp->factory($this->sConfigPath);
	}
	
	
	/**
	 * 
	 */
	public function thisTest () : void
	{
		Debug::display($this);
		Debug::display($this->aParams);
	}
	
	
	/**
	 * 
	 * @return ResponseInterface
	 */
	public function getResponse () : ResponseInterface
	{
		return $this->oResponse;
	}


	/**
	 * 
	 * @param string $psVar
	 * @param string|array|null $pmDefault
	 * @return string|array|null
	 */
	public function getRequestGet (string $psVar, $pmDefault = null)
	{
		if (isset($this->aParams['GET'][$psVar]))
		{
			return $this->aParams['GET'][$psVar];
		}
		else
		{
			return $pmDefault;
		}
	}

	
	/**
	 * 
	 * @param string $psVar
	 * @param string|array|null $pmDefault
	 * @return string|array|null
	 */
	public function getRequestPost (string $psVar, $pmDefault = null)
	{
		if (isset($this->aParams['POST'][$psVar]))
		{
			return $this->aParams['POST'][$psVar];
		}
		else
		{
			return $pmDefault;
		}
	}

	
	/**
	 * 
	 * @param string $psVar
	 * @param string|array|null $pmDefault
	 * @param bool $pbRequired
	 * @param string|null $psMsgHelp
	 * @return string|array|null
	 */
	public function getRequestArg (string $psVar, $pmDefault = null, bool $pbRequired = false, ?string $psMsgHelp = null)
	{
		if (isset($this->aParams['ARG'][$psVar]))
		{
			return $this->aParams['ARG'][$psVar];
		}
		else
		{
			if (PHP_SAPI == "cli" && PROMPT)
			{
				$lsMsgHelp = "";
				$lsMsgHelpSeparate = "";
				
				$loHelp = new Help();
				$loHelp->factory($this->sConfigPath);

				if (!empty($psMsgHelp))
				{
					$lsMsgHelp = $psMsgHelp;
					$lsMsgHelpSeparate = ". ";
				}
				else 
				{
					$lsVarHelp = $loHelp->getParamHelp($this->sModule, $this->sController, $this->sAction, $psVar);
				
					if (!empty($lsVarHelp))
					{
						$lsMsgHelp = $lsVarHelp;
						$lsMsgHelpSeparate = ". ";
					}
				}
				
				if ($pmDefault != null)
				{
					$lsMsgHelp .= "{$lsMsgHelpSeparate}Default: ({$pmDefault})";
					$lsMsgHelpSeparate = ". ";
				}
				
				if ($pbRequired)
				{
					$lsMsgHelp .= "{$lsMsgHelpSeparate}[required]";
				}
				
				if (!empty($lsMsgHelp))
				{
					echo("{$lsMsgHelp}\n");
				}
				
				$lsAnswer = Prompt::prompt("Enter param [$psVar]:");
					
				if ($this->validateValue($psVar, $lsAnswer, 'ARG'))
				{
					$this->aParams['ARG'][$psVar] = $lsAnswer;
					
					if (!empty($lsAnswer))
					{
						return $lsAnswer;
					}
					elseif (!empty($pmDefault)) 
					{
						$this->aParams['ARG'][$psVar] = $pmDefault;
						return $pmDefault;
					}
					elseif(!$pbRequired)
					{
						return null;
					}
					else 
					{
						Prompt::echoError("The param value is required to continue!");
						Prompt::echoError("Try --help!");
						Prompt::exitError("ABORTING SCRIPT EXECUTION!");
					}
				}
				else
				{
					if (empty($lsAnswer) && !empty($pmDefault))
					{
						$this->aParams['ARG'][$psVar] = $pmDefault;
						return $pmDefault;
					}
					elseif(empty($lsAnswer) && empty($pmDefault) && !$pbRequired)
					{
						return null;
					}
					else
					{
						Prompt::echoError("The param value do not match to the expected!");
						Prompt::echoError("Try --help!");
						Prompt::exitError("ABORTING SCRIPT EXECUTION!");
					}
				}
			}
			else
			{
				return $pmDefault;
			}
		}
	}

	
	/**
	 * 
	 * @param string $psVar
	 * @param string|array|null $pmDefault
	 * @param bool $pbRequired
	 * @return string|array|null
	 */
	public function getRequest (string $psVar, $pmDefault = null, bool $pbRequired = false)
	{
		return $this->getRequestArg($psVar, $this->getRequestGet($psVar, $this->getRequestPost($psVar, $pmDefault)), $pbRequired);
	}
	

	/**
	 * 
	 * @return string
	 */
	public function getVersion () : string
	{
		return Application::API_VERSION;
	}


	/**
	 * 
	 * @param string $psAction
	 */
	public function setAction (string $psAction) : void
	{
		$this->sAction = $psAction;
	}	

	
	/**
	 * 
	 * @param string|null $psTemplate
	 */
	public function setLayoutTemplate (?string $psTemplate = null) : void
	{
	    //TODO: criar template para layout
	}
	
	
	/**
	 * 
	 * @param string|null $psTemplate
	 */
	public function setViewTemplate (?string $psTemplate = null) : void
	{
	    //TODO: criar template para view
	}
	
	
	/**
	 * 
	 */
	public function setView () : void
	{
	    //TODO: criar classe view
	}
	
	
    /**
     * 
     * @param array|string $pmView
     */
	public function httpView ($pmView) : void
	{
		@header("Access-Control-Allow-Origin: " . $this->sAccessContol);
		@header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS,HEAD");
		@header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		
		if ($this->sResoponseType == 'json')
		{
			@header("Content-Type: application/json");
			echo json_encode($pmView);
		}
		elseif ($this->sResoponseType == 'stream')
		{
			@header("Content-type: text/event-stream");
			$lsData = json_encode($pmView);
			$lsResponse  = "data: " . $lsData . "\n\n";
			$lsResponse .= "retry: {$this->nStreamRetry}\n\n";
			
			echo $lsResponse;
		}
	    else 
	    {
	    	if (!empty($pmView))
	    	{
    			echo '<pre style="margin:10px;"><code><fieldset><legend>Onion Api:</legend>';
    			Debug::displayDebug($pmView);
    			echo '</fieldset></code></pre>';
	    	}
	    }
	}
}