<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi\Abstracts;
use OnionApi\Help;
use OnionApi\ServerMiddleware\RequestHandlerInterface;
use OnionHttp\HttpResponse;
use OnionLib\Debug;


abstract class AbstractView extends AbstractHandler implements RequestHandlerInterface
{

	/**
	 * @var object|\OnionApi\Abstracts\AbstractController
	 */
	protected $oController;
	
	/**
	 * @var \OnionHttp\HttpResponse
	 */
	protected $oResponse;
	
	/**
	 * @var string
	 */
	protected $sLayoutTemplate;
	
	/**
	 * @var string
	 */
	protected $sViewTemplate;
	
	/**
	 * @var string
	 */
	protected $sResoponseType = 'http'; // json, stream
	
	/**
	 * @var int
	 */
	protected $nStreamRetry = 4000;
	
	/**
	 * @var string
	 */
	protected $sAccessContol = '*';

	/**
	 * @var string
	 */
	protected $sModule;

	/**
	 * @var string
	 */
	protected $sController;

	/**
	 * @var string
	 */
	protected $sAction;

	/**
	 * @var string
	 */
	protected $sConfigPath;


	/**
	 * 
	 * @param \OnionApi\Abstracts\AbstractController $poController;
	 */
	public function __construct (AbstractController $poController)
	{
		$this->oController = $poController;
		
		$this->oResponse = $this->oController->getResponse();
		
		$lsMethod = 'init';

		if (method_exists($this, $lsMethod))
		{
			$this->{$lsMethod}();
		}
	}

	
	/**
	 * 
	 * @param bool $pbForce
	 * @param bool $lbGeneral
	 * @return string|null
	 */
	public function help (bool $pbForce = false, bool $lbGeneral = false)
	{
		if (SHOW_HELP || $pbForce)
		{			
			$loHelp = new Help();
			
			if (method_exists($this, 'moduleHelp'))
			{
				$this->moduleHelp($loHelp);
			}
			
			if (!$lbGeneral)
			{
				$laHelpContent = $loHelp->getActionHelp($this->sModule, $this->sController, $this->sAction);
			
				if (count($laHelpContent) == 0)
				{
					$laHelpContent = $loHelp->getControllerHelp($this->sModule, $this->sController);
				}
			
				$loHelp->setModuleHelp($laHelpContent);
			}
			
			return $this->oResponse->write($loHelp->display());
		}
		
		return null;
	}
	
	
	/**
	 *
	 * @param \OnionApi\Help $poHelp
	 */
	public function moduleHelp (Help $poHelp) : void
	{
		$poHelp->factory($this->sConfigPath);
	}
	
	
	/**
	 * 
	 * @param string $psTemplate
	 */
	public function setLayoutTemplate (?string $psTemplate = null) : void
	{
	    //TODO: criar template para layout
	}
	
	
	/**
	 * 
	 * @param string $psTemplate
	 */
	public function setViewTemplate (?string $psTemplate = null) : void
	{
	    //TODO: criar template para view
	}
	
	
	/**
	 * 
	 */
	public function setView () : void
	{
	    //TODO: criar classe view
	}
	
	
    /**
     * 
     * @param array|string $pmView
     */
	public function httpView ($pmView) : void
	{
		@header("Access-Control-Allow-Origin: " . $this->sAccessContol);
		@header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS,HEAD");
		@header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		
		if ($this->sResoponseType == 'json')
		{
			@header("Content-Type: application/json");
			echo json_encode($pmView);
		}
		elseif ($this->sResoponseType == 'stream')
		{
			@header("Content-type: text/event-stream");
			$lsData = json_encode($pmView);
			$lsResponse  = "data: " . $lsData . "\n\n";
			$lsResponse .= "retry: {$this->nStreamRetry}\n\n";
			
			echo $lsResponse;
		}
	    else 
	    {
	    	if (!empty($pmView))
	    	{
    			echo '<pre style="margin:10px;"><code><fieldset><legend>Onion Api:</legend>';
    			Debug::displayDebug($pmView);
    			echo '</fieldset></code></pre>';
	    	}
	    }
	}
}