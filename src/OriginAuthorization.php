<?php
/**
 * This file is part of Onion Api
 *
 * Copyright (c) 2014-2020, Humberto Lourenço <betto@m3uzz.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Humberto Lourenço nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   PHP
 * @package    OnionApi
 * @author     Humberto Lourenço <betto@m3uzz.com>
 * @copyright  2014-2020 Humberto Lourenço <betto@m3uzz.com>
 * @license    http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link       http://github.com/m3uzz/onion-api
 */
declare (strict_types = 1);

namespace OnionApi;
use OnionApi\Abstracts\AbstractHandler;
use OnionApi\ServerMiddleware\RequestHandlerInterface;
use OnionHttp\HttpRequest;
use OnionHttp\HttpResponse;
use OnionLib\Debug;
use OnionLib\Event;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class OriginAuthorization extends AbstractHandler implements RequestHandlerInterface
{
	/**
	 * @var \OnionApi\ServerMiddleware\RequestHandlerInterface
	 */
	private static $oInstance;

	/**
	 * 
	 * @var array
	 */
	protected $aRules = [ //IPs
		'*' => [
			'user-agent' => [
				'*' => ''
			]
		]
	];
	
	/**
	 * 
	 * @var array
	 */
	protected $aCredential = [
		"IP" => "",
		"UserAgent" => "",
		"Token" => "",
		"Code" => 200,
		"Message" => "Authorized"
	];


	/**
	 * 
	 */
    private function __clone ()
    {
    }


	/**
	 * @throws \Exception
	 */
    public function __wakeup () : void
    {
		throw new \Exception("Cannot unserialize a singleton.");
    }
	

	/**
	 * 
	 * @return \OnionApi\OriginAuthorization
	 */
	private function __construct ()
	{
	}


	/**
	 * 
	 * @return \OnionApi\ServerMiddleware\RequestHandlerInterface
	 */
    public static function getInstance () : RequestHandlerInterface
    {
		if (self::$oInstance === null)
		{
            self::$oInstance = new self();
		}
		
		$laAccessRules = Config::getOptions('access');

		if (is_array($laAccessRules))
		{
			self::$oInstance->aRules = $laAccessRules;
		}
		
		Debug::debug(self::$oInstance->aRules);

        return self::$oInstance;
	}		
	
	
	/**
	 * 
	 * @param ServerRequestInterface $poRequest
	 * @param ResponseInterface $poResponse
	 * @param RequestHandlerInterface|null $poNextHandler
	 * @return ResponseInterface
	 */
	public function handle (ServerRequestInterface $poRequest, ResponseInterface $poResponse, ?RequestHandlerInterface $poNextHandler = null) : ResponseInterface
	{
		if ($poResponse instanceof HttpResponse && !$poResponse->isSuccessful())
        {
			return $poResponse;
		}
		
		if (PHP_SAPI == "cli")
		{
			return parent::handle($poRequest, $poResponse, $poNextHandler);
		}

		if ($poRequest instanceof HttpRequest)
		{
			$this->aCredential['IP'] = !is_null($poRequest->getServerParam('HTTP_X_REAL_IP')) ? $poRequest->getServerParam('HTTP_X_REAL_IP') : $poRequest->getServerParam('REMOTE_ADDR');
			$this->aCredential['UserAgent'] = $poRequest->getServerParam('HTTP_USER_AGENT');
			$this->aCredential['Token'] = $poRequest->getServerParam('HTTP_TOKEN');
		}

		$lbReturn = $this->checkAddress(
			$this->aCredential['IP'], 
			$this->aCredential['UserAgent'], 
			$this->aCredential['Token']
		);
	
		Event::log(json_encode($this->aCredential), 'access');
		Debug::debug($this->aCredential);
			
		if ($lbReturn)
		{		
			//$poResponse = $this->successResponse($poRequest);
			return parent::handle($poRequest, $poResponse, $poNextHandler);
		}

		return $this->unauthorizedResponse($poRequest);
	}
	
	
	/**
	 *
	 * @param string|null $psIp
	 * @param string|null $psUserAgent
	 * @param string|null $psToken
	 * @return bool
	 */
	protected function checkAddress (?string $psIp, ?string $psUserAgent, ?string $psToken) : bool
	{
		if (isset($this->aRules[$psIp]))
		{
			return $this->checkUserAgent($psIp, $psUserAgent, $psToken);
		}
		elseif (isset($this->aRules['*']))
		{
			return $this->checkUserAgent('*', $psUserAgent, $psToken);
		}

		$this->aCredential['Code'] = 401;
		$this->aCredential['Message'] = "Address origen unauthorized";
		
		return false;
	}
	
	
	/**
	 *
	 * @param string|null $psIp
	 * @param string|null $psUserAgent
	 * @param string|null $psToken
	 * @return bool
	 */
	protected function checkUserAgent (?string $psIp, ?string $psUserAgent, ?string $psToken) : bool
	{
		if (isset($this->aRules[$psIp]['user-agent'][$psUserAgent]))
		{
			return $this->checkToken($psIp, $psUserAgent, $psToken);
		}
		elseif (isset($this->aRules[$psIp]['user-agent']['*']))
		{
			return $this->checkToken($psIp, '*', $psToken);
		}

		$this->aCredential['Code'] = 401;
		$this->aCredential['Message'] = "UserAgent unauthorized";

		return false;
	}
	
	
	/**
	 * 
	 * @param string|null $psIp
	 * @param string|null $psUserAgent
	 * @param string|null $psToken
	 * @return bool
	 */
	protected function checkToken (?string $psIp, ?string $psUserAgent, ?string $psToken) : bool
	{
		if ($this->aRules[$psIp]['user-agent'][$psUserAgent] == $psToken)
		{
			return true;
		}

		$this->aCredential['Code'] = 401;
		$this->aCredential['Message'] = "Token unauthorized";

		return false;
	}
	
	
	/**
	 *
	 * @param ServerRequestInterface $poRequest
	 * @return ResponseInterface
	 */
	protected function unauthorizedResponse (ServerRequestInterface $poRequest) : ResponseInterface
	{
		$loResponse = new HttpResponse(401);
        
        $laBody = [
            'status' => 'fail',
            'statusMessage' => [
                [
                    'method' => $poRequest->getMethod(),
                    'code' => '401',
                    'message' => [$this->aCredential['Message']]
                ]
            ],
            'data' => []
        ];
		
		$loResponse->withJson($laBody, 401);
		
		return $loResponse;
	}
	
	
	/**
	 *
	 * @param ServerRequestInterface $poRequest
	 * @return ResponseInterface
	 */
	protected function successResponse (ServerRequestInterface $poRequest) : ResponseInterface
	{
		$loResponse = new HttpResponse(200);
        
        $laBody = [
            'status' => 'success',
            'statusMessage' => [
                [
                    'method' => $poRequest->getMethod(),
                    'code' => '200',
                    'message' => [$this->aCredential['Message']]
                ]
            ],
            'data' => []
        ];
		
		$loResponse->withJson($laBody, 200);
		
		return $loResponse;
	}
}